import {DateHelper} from '../helpers/DateHelper';
import {Post} from '../Post';
import {CurrentUser} from '../User';

describe('DateHelper\'s functions:', () => {
    it('Checking user\'s age: with birthday 1965-01-2 should return 55', () => {
        let userName = new CurrentUser();
        let currentResult = userName.getAge();
        expect(currentResult).toEqual(55);
    });

    // it('Checking timePublicationAgo: with datePublic 2020-01-05 should return \'Published at 3:22 5 January\'', () => {
    //     let post = new Post();
    //     let currentResult = DateHelper.getTimeAgo(post.value.datePublic);
    //     expect(currentResult).toEqual('Published at 3:22 5 January');
    // });
});
