import {inject} from 'inversify';
import {DateHelper} from './helpers/DateHelper';
import {CurrentUser, Guard} from './User';

type DataPost = {
    content: contentData;
    datePublic: Date;
    imagePost: string;
    authorId: number;
};

type contentData = {
    header: string;
    text: string
};

let post: DataPost = {
    content: {header: 'Smth new about news', text: 'MyText'},
    datePublic: new Date('2020-01-05T01:22:00Z'),
    imagePost: 'https://bipbap.ru/wp-content/uploads/2017/04/72fqw2qq3kxh.jpg',
    authorId: 2,
};

export class Post {
    value: DataPost;
    currentUser: CurrentUser;

    constructor(@inject(CurrentUser) currentUser: CurrentUser) {
        this.value = post;
        this.currentUser = currentUser;
    }

    showPost() {
        // show post
        DateHelper.getTimeAgo(this.value.datePublic);
    }

    editPost() {
        if (((Guard.isModerator(this.currentUser.group))) ||
            (Guard.isOwner(this.currentUser.group, this.currentUser.userInfo.userId, this.value.authorId))) {
            // edit post
            console.log('editing...');
        } else {
            console.log('Permission denied for editing post');
        }
    }

    deletePost() {
        // if ((Guard.isModerator()) || (Guard.isOwner(this.group, this.CurrentUser.name, this.value.author))) {
        //     // edit post
        // } else {
        //     console.log('Permission denied');
        // }
    }
}
