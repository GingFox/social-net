export class DateHelper {
    timeAgo: string;
    datePublication: Date;

    constructor(datePublic: Date) {
        this.datePublication = datePublic;
    }

    static getTimeAgo(datePublication: Date): string {
        const currentTime = new Date();
        const timeMs = currentTime.getTime() - datePublication.getTime();
        const timeAgo = new Date(timeMs); // miliseconds from epoch
        let result: string;
        if ((timeAgo.getFullYear() - 1970) === 0) {
            if (timeAgo.getMonth() === 0) {
                if ((timeAgo.getDate() - 1) === 0) {
                    if (timeAgo.getHours() === 0) {
                        if (timeAgo.getMinutes() === 0) {
                            if (timeAgo.getSeconds() < 5) {
                                result = 'Published just now';
                            } else {
                                result = 'Published ' + timeAgo.getSeconds() + ' sec ago';
                            }
                        } else {
                            result = 'Published ' + timeAgo.getMinutes() + ' min ago';
                        }
                    } else {
                        result = 'Published ' + timeAgo.getHours() + ' hours ago';
                    }
                } else if ((timeAgo.getDate() - 1) === 1) {
                     result = 'Published yesterday';
                } else {
                    result = this.getShortDate(datePublication);
                }
            } else {
                result = this.getShortDate(datePublication);
            }
        } else {
            result = this.getfoolDate(datePublication);
        }
        return result;
    }

    static nameMonth(n: number): string {
        const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        return months[n];
    }

    static getfoolDate(date: Date): string {
        return 'Published at ' + date.getHours() + ':' + date.getMinutes() + ' ' + date.getDate() + ' ' + this.nameMonth(date.getMonth()) + ' ' + date.getFullYear();
    }

    static getShortDate(date: Date): string {
        return 'Published at ' + date.getHours() + ':' + date.getMinutes() + ' ' + date.getDate() + ' ' + this.nameMonth(date.getMonth());
    }
}
