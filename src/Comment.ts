import {DateHelper} from './helpers/DateHelper';
import { AuthService } from './authService';
import { CurrentUser } from './User';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

type DataComment = {
    authorComment: string;
    textComment: string;
    datePublic: Date;
};

let comment: DataComment = {
    authorComment: 'Inga',
    textComment: 'Comment to post',
    datePublic: new Date('20200907'),
};

export class Comment {
    value: DataComment;
    currentUserLogin$: Observable<string>;

    constructor(private authService: AuthService) {
        this.value = comment;
        this.currentUserLogin$ = this.authService.currentUser$.pipe(filter(curUser => curUser !== null), map(user => user.userInfo.login));
    }

    showComment() {
        console.log(DateHelper.getTimeAgo(this.value.datePublic));
    }

    editComment() {
    }

    deleteComment() {
    }
}
