import {injectable} from 'inversify';
import {BehaviorSubject, Observable} from 'rxjs';
import { CurrentUser } from './User';

@injectable()
export class AuthService {
    private _currentUser$: BehaviorSubject<CurrentUser> = new BehaviorSubject(null);
    public currentUser$: Observable<CurrentUser>;


    constructor() {
        this.currentUser$ = this._currentUser$.asObservable();
    }

    setCurrentUser(curUser: CurrentUser) {
        this._currentUser$.next(curUser);
    }


}
