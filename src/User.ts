import {injectable} from 'inversify';
import 'reflect-metadata';
import {DateHelper} from './helpers/DateHelper';

type userDataFields = {
    userId: number;
    login: string;
    password: string;
    lastSeen: Date;
};

let userDataNames: userDataFields = {
    userId: 1,
    login: 'igor777',
    password: '12345',
    lastSeen: new Date()
};

type personDataFields = {
    name: string;
    surname: string;
    email: string;
    phoneNumber: string;
    birthday: Date;
};

let personDataNames: personDataFields = {
    name: 'Igor',
    surname: 'Dew',
    email: 'igor@gmai.com',
    phoneNumber: '0969084756',
    birthday: new Date('1965-01-2'),
};

// interface Person {
//     personInfo: personDataFields;
// }

enum Roles {
    Admin = 1,
    Moderator = 2,
    JustUser = 3,
}

enum Rights {
    CreatePost,
    CreateComment,
    SendMessage,
    EditPost,
    DeletePost,
    EditComment,
    DeleteComment,
    BannUser,
    EditUser,
    DeleteUser,
    CreateNewUser,
    EditSettingsSystem,
    GetRights
}

export class Guard {
    permissions: (string | boolean)[][] = [];

    constructor() {
        const countRights = 14;
        const countRoles = 4;
        this.permissions[0] = [];
        for (let i = 1; i < countRoles; i++) {
            this.permissions[0][i] = Roles[i];
            for (let j = 1; j < countRights; j++) {
                this.permissions[j] = [];
                this.permissions[j][0] = Rights[j - 1];
            }
        }
        this.rightsForUser();
        this.rightsForModerator();
        this.rightsForAdmin();
        console.log(this.permissions);
    }

    static isAdmin(groups: number[]): boolean {
        let result: boolean;
        // console.log(groups);
        result = groups.some(elem => elem === 1);
        return result;
    }

    static isModerator(groups: number[]): boolean {
        let result: boolean;
        // console.log(groups);
        result = groups.some(elem => elem === 2);
        return result;
    }

    static isOwner(groups: number[], userId: number, authorId: number): boolean {
        let result: boolean;

        if (groups.some(elem => elem === 3)) {
            if (userId === authorId) {
                console.log('User is post\'s author');
                result = true;
            } else {
                console.log('User is not post\'s author');
                result = false;
            }
        }

        if (groups.some(elem => elem === 2)) {
                console.log('User is moderator');
                result = true;
            } else {
                console.log('User is not moderator');
                result = false;
            }
        return result;
    }

    rightsForAdmin() {
        for (let i = 1; i <= 7; i++) {
            this.permissions[i][1] = false;
        }
        for (let i = 8; i <= 13; i++) {
            this.permissions[i][1] = true;
        }
    }

    rightsForModerator() {
        for (let i = 1; i <= 2; i++) {
            this.permissions[i][2] = false;
        }
        for (let i = 3; i <= 8; i++) {
            this.permissions[i][2] = true;
        }
        for (let i = 9; i <= 13; i++) {
            this.permissions[i][2] = false;
        }
    }

    rightsForUser() {
        for (let i = 1; i <= 7; i++) {
            this.permissions[i][3] = true;
        }

        for (let i = 8; i <= 13; i++) {
            this.permissions[i][3] = false;
        }
    }
}

export abstract class User {
    abstract showPosts(): void;

    abstract showComments(): void;
}

@injectable()
export class CurrentUser extends User { // authorized user
    userInfo: userDataFields;
    personInfo: personDataFields;
    group: number[];

    constructor() {
        super();
        this.personInfo = personDataNames;
        this.userInfo = userDataNames;
        this.group = [];
        this.group.push(Roles.JustUser);
        this.group.push(Roles.Admin);
        // this.getRights('moderator');
    }

    getRights(access: string) {
        if (Guard.isAdmin(this.group)) {
            if (access === 'moderator') {
                this.group.push(Roles.Moderator);
                // console.log('Role was added');
            } else if (access === 'admin') {
                this.group.push(Roles.Admin);
            } else {
                console.log('Error');
            }
        } else {
            console.log('Permission denied');
        }
    }

    bannUser() {
        if (Guard.isAdmin(this.group)) {
            // bann user
        } else {
            console.log('Permission denied');
        }
    }

    editUser() {
        if (Guard.isAdmin(this.group)) {
            // edit user
        } else {
            console.log('Permission denied');
        }
    }

    deleteUser() {
        if (Guard.isAdmin(this.group)) {
            // delete user
        } else {
            console.log('Permission denied');
        }
    }

    createNewUser() {
        if (Guard.isAdmin(this.group)) {
            // create new user
        } else {
            console.log('Permission denied');
        }
    }

    editSettingsSystem() {
        if (Guard.isAdmin(this.group)) {
            // edit
        } else {
            console.log('Permission denied');
        }
    }

    showInfo() {
        console.log(this.personInfo.name, this.personInfo.surname);
        console.log(this.personInfo.birthday.getDate(),
            DateHelper.nameMonth(this.personInfo.birthday.getMonth()),
            this.personInfo.birthday.getFullYear());
        console.log('Age: ', this.getAge());
    }

    showPosts() {
    }

    showComments() {
    }

    signOut() {
    }

    sendMessage() {
    }

    createPost() {
        if (!Guard.isAdmin(this.group) && !Guard.isModerator(this.group)) {
            // create post
        } else {
            console.log('Permission denied');
        }
    }

    createComment() {
    }

    getAge(): number {
        const ageMs = Date.now() - this.personInfo.birthday.getTime();
        const ageDate = new Date(ageMs); // miliseconds from epoch
        return Math.abs(ageDate.getFullYear() - 1970);
    }
}
