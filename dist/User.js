"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Person {
    constructor(userDataNames) {
        this.name = userDataNames.name;
        this.surname = userDataNames.surname;
        this.email = userDataNames.email;
        this.phoneNumber = userDataNames.phoneNumber;
        this.login = userDataNames.login;
        this.password = userDataNames.password;
        console.log('PersonConstructor is run');
    }
    signUp() {
    }
    signOut() {
    }
    sendMessage() {
    }
    createPost() {
    }
    editPost() {
    }
    deletePost() {
    }
    createComment() {
    }
    editComment() {
    }
    deleteComment() {
    }
}
exports.Person = Person;
//# sourceMappingURL=User.js.map