"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("./User");
const Post_1 = require("./Post");
const Comment_1 = require("./Comment");
exports.userDataNames = {
    name: "Igor",
    surname: "Dew",
    email: "igor@gmai.com",
    phoneNumber: "0969084756",
    login: "igoresha",
    password: "12345",
};
exports.post = {
    content: { header: "Smth new about news", text: "MyText" },
    datePublic: "20-01-19",
    imagePost: "https://bipbap.ru/wp-content/uploads/2017/04/72fqw2qq3kxh.jpg",
    author: "Lika",
};
exports.comment = {
    authorComment: "Inga",
    textComment: "Comment to post",
    datePublic: "12-12-19",
};
let user1 = new User_1.Person(exports.userDataNames);
console.log('user1 is created');
let post1 = new Post_1.Post(exports.post);
console.log('post1 is created');
let comment1 = new Comment_1.Comment(exports.comment);
console.log('comment1 is created');
//# sourceMappingURL=main.js.map