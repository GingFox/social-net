// Karma configuration
// Generated on Mon Jan 06 2020 23:13:09 GMT+0200 (GMT+02:00)

module.exports = function (config) {
    config.set({
        basePath: '',
        frameworks: ["jasmine", "karma-typescript"],
        files: [
            {pattern: "src/**/*.ts"}
        ],
        exclude: [],
        preprocessors: {
            "**/*.ts": ["karma-typescript"]
        },
        reporters: ["dots", "karma-typescript"],
        browsers: ['Chrome'],
        reporters: ['kjhtml'],
//port: 9876,
        logLevel: config.LOG_DEBUG,
        singleRun: false,
    })
};
